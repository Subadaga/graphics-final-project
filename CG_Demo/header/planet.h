#pragma once
#ifdef __APPLE__
// See: http://lnx.cx/docs/opengl-in-xcode/
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include "freeglut.h"
#endif
#include <stdio.h>
#include <math.h>
#include "glm.h"

class planet
{
public:
	planet(double _r, double _g, double _b, double _planetSpeed, double _planetSelfSpeed, double _size, double _distance, GLMmodel* _planetModel);
	~planet();

	void draw();
	void update();

	GLMmodel* planetModel;
	double r;
	double g;
	double b;
	double planetSpeed;
	double planetSelfSpeed;
	double size;
	double distance;
	double speed = 0;
	double selfSpeed = 0;
	float		planet_dims[3];
};

