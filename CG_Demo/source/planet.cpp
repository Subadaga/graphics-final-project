#include "..\header\planet.h"



planet::planet(double _r, double _g, double _b, double _planetSpeed, double _planetSelfSpeed, double _size, double _distance, GLMmodel* _planetModel)
{

	r = _r;
	g = _g;
	b = _b;
	planetSpeed = _planetSpeed;
	planetSelfSpeed = _planetSelfSpeed;
	size = _size;
	distance = _distance;
	planetModel = _planetModel;
	glmUnitize(planetModel);
	glmFacetNormals(planetModel);
	glmDimensions(planetModel, planet_dims);
	glmScale(planetModel, size);
}


planet::~planet()
{
}

void planet::draw()
{
	//Planet Orbit
	glPushMatrix();
	glColor3f(1, 1, 1);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glutWireTorus(0.001, distance, 100, 100);
	glPopMatrix();

	//Planet
	glPushMatrix();

	glColor3f(r, g,b);

	// Rotation of the planet around the sun
	glRotatef(speed, 0, 1, 0);
	glTranslatef(distance, 0, 0);

	// Self-Rotation of the planet
	glRotatef(selfSpeed, 0, 1, 0);

	glmDraw(planetModel, GLM_SMOOTH | GLM_TEXTURE);
	glPopMatrix();

}

void planet::update()
{
	speed += planetSpeed;
	selfSpeed += planetSelfSpeed;
}
