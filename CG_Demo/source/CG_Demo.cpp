/*

Sergio Sanchez Basoco
Cristian Geovanni Aguilar Valencia
Abner Axel Lopez Ninio
Oscar Antonio Becerril Lopez
.
 */
#ifdef __APPLE__
 // See: http://lnx.cx/docs/opengl-in-xcode/
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include "freeglut.h"
#endif
#include <stdio.h>
#include <math.h>
#include <planet.h>
#include "glm.h"


float		rotation;
float		tilt;
GLMmodel* plane;
GLMmodel*	sintel;
GLMmodel*   saturn;
GLMmodel* earth;
GLMmodel* uranus;
GLfloat*	global_ambient;

GLMmodel* mercuryModel;
GLMmodel* venusModel;
GLMmodel* marsModel;
GLMmodel* jupiterModel;
GLMmodel* neptuneModel;


float		sintel_pos[3];
float		sintel_dims[3];
float		radius;

planet* mercury;
planet* venus;
planet* mars;
planet* jupiter;
planet* neptune;

// Special planet speeds

double earthSpeed = 0;
double moonSpeed = 0;
double saturnSpeed = 0;
double uranusSpeed = 0;


// Special planet self speeds

double sunSelfSpeed = 0;
double earthSelfSpeed = 0;
double moonSelfSpeed = 0;
double saturnSelfSpeed = 0;
double uranusSelfSpeed = 0;

// Camera position
double camX = 0;
double camY = 40;
double camZ = -1.0;

void init() // FOR GLUT LOOP
{
	glEnable(GL_DEPTH_TEST);			// Enable check for close and far objects.
	glClearColor(0.0, 0.0, 0.0, 0.0);	// Clear the color state.
	glMatrixMode(GL_MODELVIEW);			// Go to 3D mode.
	glLoadIdentity();					// Reset 3D view matrix.
	mercuryModel = glmReadOBJ("assets/test.obj");
	venusModel = glmReadOBJ("assets/venusTest.obj");
	marsModel = glmReadOBJ("assets/marsTest.obj");
	jupiterModel = glmReadOBJ("assets/jupiterTest.obj");
	neptuneModel = glmReadOBJ("assets/neptuneTest.obj");
	mercury = new planet(0.66, 0.66, 0.66, 5, 2, 0.1, 1, mercuryModel);
	venus = new planet(0.824, 0.412, 0.118, 3, 2, 0.2, 1.5, venusModel);
	mars = new planet(0.871, 0.722, 0.529, 2.5, 2, 0.23, 4, marsModel);
	jupiter = new planet(1, 0.894, 0.769, 0.5, 2, 0.5, 8, jupiterModel);
	neptune = new planet(0.255, 0.412, 0.882, 0.1, 2, 0.2, 14, neptuneModel);

	rotation = 90.0f;
	tilt = 0.0f;
	radius = 0;
	sintel_pos[0] = 0.0f;
	sintel_pos[1] = 10.0f;
	sintel_pos[2] = 0.0f;

	plane = glmReadOBJ("assets/plane.obj");
	glmUnitize(plane);
	glmFacetNormals(plane);
	glmDimensions(plane, sintel_dims);
	glmScale(plane, 20);

	earth = glmReadOBJ("assets/earthTest.obj");
	glmUnitize(earth);
	glmFacetNormals(earth);
	glmDimensions(earth, sintel_dims);
	glmScale(earth, 0.21);

	saturn = glmReadOBJ("assets/saturnTest.obj");
	glmUnitize(saturn);
	glmFacetNormals(saturn);
	glmDimensions(saturn, sintel_dims);
	glmScale(saturn, 0.35);

	uranus = glmReadOBJ("assets/uranusTest.obj");
	glmUnitize(uranus);
	glmFacetNormals(uranus);
	glmDimensions(uranus, sintel_dims);
	glmScale(uranus, 0.35);

	sintel = glmReadOBJ("assets/ship.obj");
	glmUnitize(sintel);
	glmFacetNormals(sintel);
	glmDimensions(sintel, sintel_dims);
	float center[3] = { sintel_pos[0] + sintel_dims[0] / 2.0f,
		sintel_pos[1] + sintel_dims[1] / 2.0f,
		sintel_pos[2] + sintel_dims[2] / 2.0f };

	radius = sqrtf(center[0] * center[0] +
		center[1] * center[1] +
		center[2] * center[2]);

	printf("SINTEL_DIMS{%.3f, %.3f, %.3f}\n", sintel_dims[0], sintel_dims[1], sintel_dims[2]);
	printf("RADIUS=%.3f\n", radius);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glClearColor(0.0, 0.0, 0.0, 0.0);

	glLoadIdentity();
	gluLookAt(0, 40, -1, 0, 0, 0, 0, 1, 0);

	glEnable(GL_LIGHTING);
	GLfloat light_position[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat light_ambient_color[] = { 0.0, 0.0, 0.0, 0.0 };
	GLfloat light_diffuse_color[] = { 1.0, 1.0, 1.0, 1.0 };

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient_color);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse_color);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);





}

void display()							// Called for each frame (about 60 times per second).
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);				// Clear color and depth buffers.



	glPushMatrix();
	glTranslatef(sintel_pos[0], sintel_pos[1], sintel_pos[2]);
	glRotatef(rotation, 0.0f, 0.1f, 0.0f);
	glRotatef(tilt, 0.10f, 0.0f, 0.0f);
	glmDraw(sintel, GLM_SMOOTH | GLM_TEXTURE);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0, -1, 0);
	glmDraw(plane, GLM_SMOOTH | GLM_TEXTURE);
	glPopMatrix();


	glPushMatrix();
	GLfloat mat_em[] = { 1.0, 0.843, 0.0, 0.0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_em);
	glColor3f(1, 1, 0);
	glRotatef(sunSelfSpeed, 0, 1, 0);
	glutSolidSphere(0.5, 100, 100);
	GLfloat r_emission[4] = { 0.0, 0.0, 0.0, 1.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, r_emission);
	glPopMatrix();

	mercury->draw();
	venus->draw();

	//Earth, special case

	glPushMatrix();
	glColor3f(1, 1, 1);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glutWireTorus(0.001, 3, 100, 100);
	glPopMatrix();

	glPushMatrix();
	// Rotation of the planet around the sun
	glRotatef(earthSpeed, 0, 1, 0);
	glTranslatef(3, 0, 0);

	// Rotation of the moon itself

	glPushMatrix();

	// Rotation of moon around the planet in question

	glRotatef(moonSpeed, 0.0, 1.0, 0.0);
	glTranslatef(0.5, 0.0, 0.0);

	// Self-rotation of the moon

	glRotatef(moonSelfSpeed, 0, 1, 0);
	glColor3f(0.753, 0.753, 0.753);
	glutSolidSphere(0.1, 100, 100);
	glPopMatrix();

	// Drawing of the moon orbit

	glPushMatrix();
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glutWireTorus(0.001, 0.5, 100, 100);
	glPopMatrix();

	// Self-Rotation of the planet
	glRotatef(earthSelfSpeed, 0, 1, 0);
	glColor3f(0, 0, 0.804);
	glmDraw(earth, GLM_SMOOTH | GLM_TEXTURE);
	glPopMatrix();

	mars->draw();
	jupiter->draw();
	neptune->draw();

	//Saturn, special case

	glPushMatrix();
	glColor3f(1, 1, 1);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glutWireTorus(0.001, 10, 100, 100);
	glPopMatrix();

	glPushMatrix();

	// Rotation of the planet around the sun
	glRotatef(saturnSpeed, 0, 1, 0);
	glTranslatef(10, 0, 0);


	// Drawing of the rings

	glPushMatrix();
	glColor3f(0.663, 0.663, 0.663);
	glRotatef(45.0, 1.0, 0.0, 0.0);
	glutWireTorus(0.04, 0.7, 100, 100);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.663, 0.663, 0.663);
	glRotatef(45.0, 1.0, 0.0, 0.0);
	glutWireTorus(0.04, 0.9, 100, 100);
	glPopMatrix();

	// Self-Rotation of the planet
	glRotatef(saturnSelfSpeed, 0, 1, 0);
	glColor3f(0.737, 0.561, 0.561);
	glmDraw(saturn, GLM_SMOOTH | GLM_TEXTURE);
	glPopMatrix();

	//Uranus, special case

	glPushMatrix();
	glColor3f(1, 1, 1);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glutWireTorus(0.001, 12, 100, 100);
	glPopMatrix();

	glPushMatrix();

	// Rotation of the planet around the sun
	glRotatef(uranusSpeed, 0, 1, 0);
	glTranslatef(12, 0, 0);


	// Drawing of the ring

	glPushMatrix();
	glColor3f(0.663, 0.663, 0.663);
	glutWireTorus(0.02, 0.7, 100, 100);
	glPopMatrix();

	// Self-Rotation of the planet
	glRotatef(uranusSelfSpeed, 0, 1, 0);
	glColor3f(0.678, 0.847, 0.902);
	glmDraw(uranus, GLM_SMOOTH | GLM_TEXTURE);
	glPopMatrix();




	glutSwapBuffers();												// Swap the hidden and visible buffers.
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w':

		sintel_pos[2] += (0.1f * sinf(fmod(rotation, 360.0)*(M_PI / 180)));
		sintel_pos[0] += (-0.1f * cosf(fmod(rotation, 360.0)*(M_PI / 180)));
		break;
	case 's':
		sintel_pos[2] -= (0.1f * sinf(fmod(rotation, 360.0)*(M_PI / 180)));
		sintel_pos[0] -= (-0.1f * cosf(fmod(rotation, 360.0)*(M_PI / 180)));
		break;
	case 'a':
		sintel_pos[2] += (0.1f * sinf(fmod(rotation, 360.0)*(M_PI / 180)));
		sintel_pos[0] += (-0.1f * cosf(fmod(rotation, 360.0)*(M_PI / 180)));
		rotation += 5;
		tilt += 2.0f;
		break;
	case 'd':
		sintel_pos[2] += (0.1f * sinf(fmod(rotation, 360.0)*(M_PI / 180)));
		sintel_pos[0] += (-0.1f * cosf(fmod(rotation, 360.0)*(M_PI / 180)));
		rotation -= 5;
		tilt -= 2.0f;
		break;
	}
}


void idle()															// Called when drawing is finished.
{
	sunSelfSpeed += 2;
	mercury->update();
	venus->update();
	earthSpeed += 2;
	earthSelfSpeed += 2;
	moonSpeed += 2;
	moonSelfSpeed += 2;
	mars->update();
	jupiter->update();
	saturnSpeed += 0.3;
	saturnSelfSpeed += 2;
	uranusSpeed += 0.2;
	uranusSelfSpeed += 2;
	neptune->update();

	sunSelfSpeed += 1;

	if (rotation > 360.0f)
	{
		rotation = 0.0f;
	}

	if (tilt != 0.0f)
	{
		if (tilt >= 40.0f)
		{
			tilt = 40.0f;
		}
		else if (tilt <= -40.0f)
		{
			tilt = -40.0f;
		}
		if (tilt > 0.0f)
		{
			tilt -= 0.2f;
		}
		if (tilt < 0.0f)
		{
			tilt += 0.2f;
		}
	}

	glutPostRedisplay();											// Display again.
}

void reshape(int x, int y)											// Called when the window geometry changes.
{
	glMatrixMode(GL_PROJECTION);									// Go to 2D mode.
	glLoadIdentity();												// Reset the 2D matrix.
	gluPerspective(40.0, (GLdouble)x / (GLdouble)y, 0.5, 50.0);		// Configure the camera lens aperture.
	glMatrixMode(GL_MODELVIEW);										// Go to 3D mode.
	glLoadIdentity();												// Reset the 3D matrix.
	glViewport(0, 0, x, y);											// Configure the camera frame dimensions.
	gluLookAt(camX, camY, camZ,										// Where the camera is.
		0.0, 0.0, 0.0,												// To where the camera points at.
		0.0, 1.0, 0.0);												// "UP" vector.
	display();
}

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);											// Init GLUT with command line parameters.
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);		// Use 2 buffers (hidden and visible). Use the depth buffer. Use 3 color channels.
	glutInitWindowSize(1920, 1080);
	glutCreateWindow("CG DEMO");

	init();
	glutReshapeFunc(reshape);										// Reshape CALLBACK function.
	glutKeyboardFunc(keyboard);
	glutDisplayFunc(display);										// Display CALLBACK function.
	glutIdleFunc(idle);												// Idle CALLBACK function.
	glutMainLoop();													// Begin graphics program.
	return 0;														// ANSI C requires a return value.
}
